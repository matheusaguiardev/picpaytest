package com.picpay.desafio.android

import androidx.lifecycle.Lifecycle
import androidx.test.core.app.launchActivity
import androidx.test.platform.app.InstrumentationRegistry
import com.picpay.desafio.android.apimock.ApiMockTest
import com.picpay.desafio.android.robot.homeTest
import com.picpay.desafio.android.ui.home.MainActivity
import org.junit.Test

class MainActivityTest: ApiMockTest() {

    @Test
    fun shouldDisplayTitle() {
        launchActivity<MainActivity>().apply {
            homeTest {
                moveToState(Lifecycle.State.RESUMED)
                waitLoading()
                checkTitleText()
                checkTitleIsVisible()
            }
        }
    }

    @Test
    fun shouldDisplayListItem() {
        launchActivity<MainActivity>().apply {
            // "validate if list displays items returned by server"
            homeTest {
                moveToState(Lifecycle.State.RESUMED)
                waitLoading()
                rollAndVerifyLastUser()
            }
        }
    }

}