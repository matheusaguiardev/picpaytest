package com.picpay.desafio.android.app

import android.app.Application
import com.picpay.desafio.android.common.myTestModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class PicPayTestApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            modules(myTestModule)
            androidContext(this@PicPayTestApp)
        }
    }

}