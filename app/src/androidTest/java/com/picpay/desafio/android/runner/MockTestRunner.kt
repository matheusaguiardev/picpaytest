package com.picpay.desafio.android.runner

import android.app.Application
import android.content.Context
import androidx.test.runner.AndroidJUnitRunner
import com.picpay.desafio.android.app.PicPayTestApp

class MockTestRunner: AndroidJUnitRunner() {
    override fun newApplication(cl: ClassLoader?, className: String?,
                                context: Context?): Application {
        return super.newApplication(cl, PicPayTestApp::class.java.name, context)
    }
}