package com.picpay.desafio.android.robot

import com.picpay.desafio.android.R
import com.picpay.desafio.android.common.getMockUserListUi


fun homeTest(func: HomeRobot.() -> Unit) = HomeRobot()
    .apply { func() }

class HomeRobot: BaseRobotTest() {

    fun checkTitleText() = matchText(textView(R.id.titleTextView), R.string.title)

    fun checkTitleIsVisible() = isDisplayed(textView(R.id.titleTextView))

    fun rollAndVerifyLastUser() = checkRecyclerViewItem(
        recycleView(R.id.recyclerView),
        0,
        getMockUserListUi().first().name
    )

}