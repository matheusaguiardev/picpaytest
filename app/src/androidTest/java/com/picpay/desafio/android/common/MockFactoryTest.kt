package com.picpay.desafio.android.common

import com.picpay.desafio.android.data.mapper.toDomain
import com.picpay.desafio.android.data.model.UserData

fun getMockUserDataListUi() = listOf(
        UserData(img = "https://randomuser.me/api/portraits/men/1.jpg", name = "John doe 1", id = 0, username = "@johndoe1"),
        UserData(img = "https://randomuser.me/api/portraits/men/2.jpg", name = "John doe 2", id = 1, username = "@johndoe2"),
        UserData(img = "https://randomuser.me/api/portraits/men/4.jpg", name = "John doe 3", id = 2, username = "@johndoe3"),
        UserData(img = "https://randomuser.me/api/portraits/men/5.jpg", name = "John doe 4", id = 3, username = "@johndoe4"),
        UserData(img = "https://randomuser.me/api/portraits/men/9.jpg", name = "John doe 5", id = 4, username = "@johndoe5")
)

fun getMockUserListUi() = getMockUserDataListUi().map { it.toDomain() }