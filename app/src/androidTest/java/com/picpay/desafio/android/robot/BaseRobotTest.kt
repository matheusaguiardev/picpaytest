package com.picpay.desafio.android.robot

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewInteraction
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.espresso.matcher.ViewMatchers.withText
import com.picpay.desafio.android.helper.RecyclerViewMatchers.atPosition

open class BaseRobotTest {

    fun textView(resId: Int): ViewInteraction = onView(withId(resId))

    fun recycleView(resId: Int): ViewInteraction = onView(withId(resId))

    fun matchText(viewInteraction: ViewInteraction, text: Int): ViewInteraction = viewInteraction
        .check(matches(withText(text)))

    fun isDisplayed(viewInteraction: ViewInteraction): ViewInteraction = viewInteraction
        .check(matches(ViewMatchers.isDisplayed()))

    fun checkRecyclerViewItem(recycleView: ViewInteraction, position: Int, text: String) {
        recycleView.check(
            matches(
                atPosition(
                    position,
                    ViewMatchers.hasDescendant(withText(text))
                )
            )
        )
    }

    fun waitLoading() {
        Thread.sleep(1000)
    }

}