package com.picpay.desafio.android.common

import com.picpay.desafio.android.app.di.repositoryModule
import com.picpay.desafio.android.app.di.viewModelModule
import com.picpay.desafio.android.app.remoteModuleTest

val myTestModule = listOf(
    remoteModuleTest,
    repositoryModule,
    viewModelModule
)