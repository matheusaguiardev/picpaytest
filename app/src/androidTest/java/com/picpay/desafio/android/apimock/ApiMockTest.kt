package com.picpay.desafio.android.apimock

import com.google.gson.GsonBuilder
import com.picpay.desafio.android.common.getMockUserDataListUi
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Before

open class ApiMockTest {

    private val server = MockWebServer()
    private val serverPort = 8080

    private val successResponse by lazy {
        val body = GsonBuilder().create().toJson(getMockUserDataListUi())

        MockResponse()
            .setResponseCode(200)
            .setBody(body)
    }

    private val errorResponse by lazy { MockResponse().setResponseCode(404) }

    @Before
    fun setUp() {
        server.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when (request.path) {
                    "/users" -> successResponse
                    else -> errorResponse
                }
            }
        }

        server.start(serverPort)
    }


    @After
    fun turnOff() {
        server.close()
    }

}