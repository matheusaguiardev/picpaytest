package com.picpay.desafio.android.data.viewmodel

import com.picpay.desafio.android.common.getMockUserList
import com.picpay.desafio.android.common.getOrAwaitValue
import com.picpay.desafio.android.domain.outcome.Outcome
import com.picpay.desafio.android.domain.repository.PicPayRepository
import com.picpay.desafio.android.ui.home.MainViewModel
import io.mockk.coEvery
import io.mockk.mockk
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config
import java.io.IOException

@RunWith(RobolectricTestRunner::class)
@Config(manifest= Config.NONE)
class MainViewModelTest {

    private val repository = mockk<PicPayRepository>(relaxed = true)
    private val viewModel = MainViewModel(repository)

    @Test fun `fetch list of users on the repository successfully`() = runBlocking {
        // Given
        coEvery { repository.getUsers() } answers { Outcome.Success(getMockUserList()) }

        // When
        viewModel.fetchUsers()

        //Then
        assertTrue(viewModel.userList().getOrAwaitValue().isNotEmpty())
        assertFalse(viewModel.errorService().getOrAwaitValue())
    }

    @Test fun `fetch user list on repository with error`() = runBlocking {
        // Given
        coEvery { repository.getUsers() } answers { Outcome.Error(IOException("Error mock"), 400) }

        // When
        viewModel.fetchUsers()

        //Then
        assertTrue(viewModel.errorService().getOrAwaitValue())
    }

}