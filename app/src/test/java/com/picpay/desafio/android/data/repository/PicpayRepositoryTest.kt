package com.picpay.desafio.android.data.repository

import com.picpay.desafio.android.common.ApiMockBaseTest
import com.picpay.desafio.android.common.getMockUserDataList
import com.picpay.desafio.android.data.remote.PicPayService
import com.picpay.desafio.android.domain.model.User
import com.picpay.desafio.android.domain.outcome.Outcome
import kotlinx.coroutines.runBlocking
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.buffer
import okio.source
import org.junit.After
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import java.nio.charset.StandardCharsets

@RunWith(JUnit4::class)
class PicpayRepositoryTest: ApiMockBaseTest() {

    private val api = createWebService<PicPayService>()

    private val repository = PicPayDataRepository(api)
    private lateinit var expectedUsers: List<User>
    private var errorOutcome: Outcome<Exception>? = null

    @Before fun setup() {
        expectedUsers = emptyList()
        errorOutcome = null
    }

    @After
    fun tearDown() {
        mockWebServer.shutdown()
    }

    @Test fun `fetch list of users on the server successfully`() = runBlocking {
        // Given
        mockWebServer.enqueueResponse(getMockUserDataList(), 200)

        // when
        expectedUsers = (repository.getUsers() as Outcome.Success).data

        // then
        assertTrue(expectedUsers.isNotEmpty() )
        assertTrue(errorOutcome == null)
    }

    @Test fun `fetch user list on server with error`() = runBlocking {
        // Given
        mockWebServer.enqueueResponse(getMockUserDataList(), 404)

        // when
        errorOutcome = repository.getUsers() as Outcome.Error

        // then
        assertTrue(expectedUsers.isEmpty())
        assertTrue((errorOutcome as Outcome.Error).httpCode == 404)
    }


}