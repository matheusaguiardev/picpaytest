package com.picpay.desafio.android.common

import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

open class ApiMockBaseTest {

    val mockWebServer = MockWebServer()

    val client = OkHttpClient.Builder()
        .connectTimeout(1, TimeUnit.SECONDS)
        .readTimeout(1, TimeUnit.SECONDS)
        .writeTimeout(1, TimeUnit.SECONDS)
        .build()

    inline fun <reified T> createWebService(): T {
        val retrofit = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(client)
            .build()
        return retrofit.create(T::class.java)
    }

    internal fun <T> MockWebServer.enqueueResponse(expected: T, code: Int) {
        val source = GsonBuilder().create().toJson(expected)
        source?.let {
            enqueue(
                MockResponse()
                    .setResponseCode(code)
                    .setBody(source)
            )
        }
    }

}