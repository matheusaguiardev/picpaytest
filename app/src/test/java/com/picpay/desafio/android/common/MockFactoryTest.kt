package com.picpay.desafio.android.common

import com.picpay.desafio.android.data.mapper.toDomain
import com.picpay.desafio.android.data.model.UserData

fun getMockUserDataList() = listOf(
        UserData(img = "url1", name = "John doe 1", id = 0, username = "@johndoe1"),
        UserData(img = "url2", name = "John doe 2", id = 1, username = "@johndoe2"),
        UserData(img = "url3", name = "John doe 3", id = 2, username = "@johndoe3"),
        UserData(img = "url4", name = "John doe 4", id = 3, username = "@johndoe4"),
        UserData(img = "url5", name = "John doe 5", id = 4, username = "@johndoe5")
)

fun getMockUserList() = getMockUserDataList().map { it.toDomain() }