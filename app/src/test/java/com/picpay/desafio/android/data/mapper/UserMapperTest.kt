package com.picpay.desafio.android.data.mapper

import com.picpay.desafio.android.data.model.UserData
import org.junit.Assert.assertTrue
import org.junit.Test

class UserMapperTest {

    private val sampleUrl = "url"
    private val sampleName = "John Doe"
    private val sampleId = 0
    private val sampleUserName = "@johndoe"

    @Test fun `conversion of userData to user check`() {
        // Given
        val userData = UserData(img = sampleUrl, name = sampleName, id = sampleId, username = sampleUserName)

        // When
        val user = userData.toDomain()

        // Then
        assertTrue(user.img == sampleUrl)
        assertTrue(user.name == sampleName)
        assertTrue(user.id == sampleId)
        assertTrue(user.username == sampleUserName)
    }

    @Test fun `conversion of userData to user with empty fields`() {
        // Given
        val userData = UserData(img = null, name = null, id = null, username = null)

        // When
        val user = userData.toDomain()

        // Then
        assertTrue(user.img.isEmpty())
        assertTrue(user.name.isEmpty())
        assertTrue(user.id == -1)
        assertTrue(user.username.isEmpty())
    }

}