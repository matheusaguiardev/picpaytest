package com.picpay.desafio.android.domain.repository

import com.picpay.desafio.android.domain.outcome.Outcome
import com.picpay.desafio.android.domain.model.User

interface PicPayRepository {

    suspend fun getUsers(): Outcome<List<User>>

}