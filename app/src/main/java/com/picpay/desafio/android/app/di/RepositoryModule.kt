package com.picpay.desafio.android.app.di

import com.picpay.desafio.android.data.repository.PicPayDataRepository
import com.picpay.desafio.android.domain.repository.PicPayRepository
import org.koin.dsl.module

val repositoryModule = module {
    single<PicPayRepository> { PicPayDataRepository(get()) }
}