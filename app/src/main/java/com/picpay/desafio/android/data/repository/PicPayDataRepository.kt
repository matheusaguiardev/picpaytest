package com.picpay.desafio.android.data.repository

import com.picpay.desafio.android.data.mapper.toDomain
import com.picpay.desafio.android.data.remote.PicPayService
import com.picpay.desafio.android.domain.model.User
import com.picpay.desafio.android.domain.outcome.Outcome
import com.picpay.desafio.android.domain.repository.PicPayRepository
import com.picpay.desafio.android.ui.utils.Constants.HTTP_STATUS_NOT_FOUND
import com.picpay.desafio.android.ui.utils.Constants.HTTP_STATUS_SUCCESS
import com.picpay.desafio.android.ui.utils.Constants.TXT_ERROR_REQUEST
import com.picpay.desafio.android.ui.utils.Constants.TXT_LIST_NOTFOUND
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.IOException

class PicPayDataRepository(
    private val api: PicPayService
) : PicPayRepository {

    override suspend fun getUsers(): Outcome<List<User>> = withContext(Dispatchers.IO) {
        val result = api.getUsers()
        when (result.code()) {
            HTTP_STATUS_SUCCESS -> Outcome.Success(
                    result.body()?.map { it.toDomain() } ?: emptyList())
            HTTP_STATUS_NOT_FOUND -> Outcome.Error(IOException(TXT_LIST_NOTFOUND), result.code())
            else -> Outcome.Error(IOException(TXT_ERROR_REQUEST), result.code())
        }
    }

}