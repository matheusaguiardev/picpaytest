package com.picpay.desafio.android.app.di

val myModule = listOf(
        remoteModule,
        repositoryModule,
        viewModelModule
)