package com.picpay.desafio.android.ui.home

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.picpay.desafio.android.R
import com.picpay.desafio.android.databinding.ActivityMainBinding
import com.picpay.desafio.android.domain.model.User
import com.picpay.desafio.android.ui.adapter.UserListAdapter
import com.picpay.desafio.android.ui.extension.toGone
import com.picpay.desafio.android.ui.extension.toVisible
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val adapter: UserListAdapter by lazy { UserListAdapter() }
    private val viewModel: MainViewModel by viewModel()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        with(viewModel) {
            fetchUsers()
            userList().observe(this@MainActivity, Observer(::usersDataObserver))
            errorService().observe(this@MainActivity, Observer(::finishRequest))
            loadingControl().observe(this@MainActivity, Observer(::showLoading))
        }
    }

    override fun onStart() {
        super.onStart()
        setupUserList()
    }

    override fun onResume() {
        super.onResume()
        adapter.users = viewModel.userList().value ?: emptyList()
    }

    private fun setupUserList() {
        with(binding.recyclerView) {
            adapter = this@MainActivity.adapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }
    }

    private fun usersDataObserver(userList: List<User>) {
        adapter.users = userList
        setEmptyState(userList.isEmpty())
    }

    private fun finishRequest(errorState: Boolean) {
        if (errorState) {
            val message = getString(R.string.error)
            binding.userListProgressBar.toGone()
            binding.recyclerView.toGone()
            Toast.makeText(this@MainActivity, message, Toast.LENGTH_SHORT)
                .show()
            setEmptyState(true)
        }
    }

    private fun setEmptyState(isEmptyList: Boolean) {
        if (isEmptyList) {
            binding.titleTextView.text = getString(R.string.txt_contact_not_found)
        } else {
            binding.titleTextView.text = getString(R.string.title)
        }
    }

    private fun showLoading(show: Boolean) {
        if (show) {
            binding.userListProgressBar.toVisible()
        } else {
            binding.userListProgressBar.toGone()
        }
    }

}
