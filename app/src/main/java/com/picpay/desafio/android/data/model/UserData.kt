package com.picpay.desafio.android.data.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class UserData(
        @SerializedName("img") val img: String?,
        @SerializedName("name") val name: String?,
        @SerializedName("id") val id: Int?,
        @SerializedName("username") val username: String?
) : Parcelable {
        constructor(parcel: Parcel) : this(
                parcel.readString(),
                parcel.readString(),
                parcel.readValue(Int::class.java.classLoader) as? Int,
                parcel.readString()
        ) {
        }

        override fun writeToParcel(parcel: Parcel, flags: Int) {
                parcel.writeString(img)
                parcel.writeString(name)
                parcel.writeValue(id)
                parcel.writeString(username)
        }

        override fun describeContents(): Int {
                return 0
        }

        companion object CREATOR : Parcelable.Creator<UserData> {
                override fun createFromParcel(parcel: Parcel): UserData {
                        return UserData(parcel)
                }

                override fun newArray(size: Int): Array<UserData?> {
                        return arrayOfNulls(size)
                }
        }
}