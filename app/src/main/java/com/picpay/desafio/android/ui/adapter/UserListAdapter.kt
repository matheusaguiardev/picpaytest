package com.picpay.desafio.android.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.picpay.desafio.android.R
import com.picpay.desafio.android.databinding.ListItemUserBinding
import com.picpay.desafio.android.ui.utils.UserListDiffCallback
import com.picpay.desafio.android.ui.home.UserListItemViewHolder
import com.picpay.desafio.android.domain.model.User

class UserListAdapter : RecyclerView.Adapter<UserListItemViewHolder>() {

    private lateinit var binding: ListItemUserBinding

    var users = emptyList<User>()
        set(value) {
            val result = DiffUtil.calculateDiff(
                    UserListDiffCallback(
                            field,
                            value
                    )
            )
            result.dispatchUpdatesTo(this)
            field = value
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserListItemViewHolder {
        binding = ListItemUserBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )

        return UserListItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: UserListItemViewHolder, position: Int) {
        holder.bind(users[position])
    }

    override fun getItemCount(): Int = users.size
}