package com.picpay.desafio.android.app.di

import com.google.gson.GsonBuilder
import com.picpay.desafio.android.data.remote.PicPayService
import com.picpay.desafio.android.ui.utils.Constants.BASE_URL
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val remoteModule = module {
    single { createOkHttpClient() }
    single { createWebService<PicPayService>(get()) }
}

fun createOkHttpClient(): OkHttpClient {
    return OkHttpClient.Builder()
            .build()
}

inline fun <reified T> createWebService(okHttpClient: OkHttpClient): T {
    val retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(GsonBuilder().create()))
            .client(okHttpClient)
            .build()
    return retrofit.create(T::class.java)
}