package com.picpay.desafio.android.app

import android.app.Application
import com.picpay.desafio.android.app.di.myModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

open class PicPayApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            modules(myModule)
            androidContext(this@PicPayApp)
        }
    }

}