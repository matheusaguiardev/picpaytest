package com.picpay.desafio.android.domain.outcome

sealed class Outcome<out T> {

    data class Success<out T>(val data: T) : Outcome<T>()
    data class Error(val exception: Exception, val httpCode: Int) : Outcome<Nothing>()

    override fun toString(): String {
        return when (this) {
            is Success<*> -> "Success[data=$data]"
            is Error -> "Error[exception=$exception]"
        }
    }
}
