package com.picpay.desafio.android.ui.utils

object Constants {

    const val BASE_URL = "https://609a908e0f5a13001721b74e.mockapi.io/picpay/api/"
    const val TXT_ERROR_REQUEST = "Falha ao realizar a requisição"
    const val TXT_LIST_NOTFOUND = "Nenhum usuário foi encontrado"

    const val HTTP_STATUS_SUCCESS = 200
    const val HTTP_STATUS_NOT_FOUND = 404

}