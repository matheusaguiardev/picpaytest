package com.picpay.desafio.android.data.helpers

import android.util.Log

inline fun <T> safeCast(block: () -> T): T? = try {
    block.invoke()
} catch (e: ClassCastException) {
    Log.d("SafeCast", "${ e.printStackTrace() }")
    null
}