package com.picpay.desafio.android.data.mapper

import com.picpay.desafio.android.data.model.UserData
import com.picpay.desafio.android.domain.model.User

fun UserData.toDomain() = User(
        img = this.img ?: "",
        name = this.name ?: "",
        id = this.id ?: -1,
        username = this.username ?: ""
)