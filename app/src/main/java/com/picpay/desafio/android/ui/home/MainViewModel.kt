package com.picpay.desafio.android.ui.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.picpay.desafio.android.domain.model.User
import com.picpay.desafio.android.domain.outcome.Outcome
import com.picpay.desafio.android.domain.repository.PicPayRepository
import kotlinx.coroutines.launch

class MainViewModel(
    private val repository: PicPayRepository
) : ViewModel() {

    private val _userList = MutableLiveData<List<User>>()
    fun userList(): LiveData<List<User>> = _userList

    private val _errorStatus = MutableLiveData<Boolean>()
    fun errorService(): LiveData<Boolean> = _errorStatus

    private val _loadingControl = MutableLiveData<Boolean>()
    fun loadingControl(): LiveData<Boolean> = _loadingControl

    fun fetchUsers() {
        _loadingControl.postValue(true)
        viewModelScope.launch {
            when (val result = repository.getUsers()) {
                is Outcome.Success -> {
                    _userList.postValue(result.data)
                    _errorStatus.postValue(false)
                    _loadingControl.postValue(false)
                }
                is Outcome.Error -> {
                    _errorStatus.postValue(true)
                    _loadingControl.postValue(false)
                }
            }
        }
    }

}